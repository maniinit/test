apiVersion: batch/v1beta1
kind: CronJob
metadata:
  labels:
    app: docker-dind
  namespace: rapid-uat
  name: cronjob
spec:
  schedule: "*/30 * * * *"
  successfulJobsHistoryLimit: 5
  failedJobsHistoryLimit: 5
  concurrencyPolicy: Forbid
  jobTemplate:
    metadata:
      labels:
        app: docker-dind
      name: docker-dind-clear-cache
    spec:
      template:
        spec:
          nodeSelector:
            app: "rapid"
          tolerations:
          - key: "app"
            operator: "Equal"
            value: "rapid"
            effect: "NoSchedule"
          - key: "node-role.kubernetes.io/master"
            operator: "Exists"
            effect: "NoSchedule"
          - key: "node.kubernetes.io/unreachable"
            operator: "Exists"
            effect: "NoSchedule"
          - key: "node.kubernetes.io/not-ready"
            operator: "Exists"
            effect: "NoSchedule"
          containers:
          - name: docker-dind-clear-cache
            image: artifactory.idfcfirstbank.com/docker-dockerhub-remote/simplifisupport/rapid-cronjob:3.4
            volumeMounts:
            - name: pydocuments-data
              mountPath: /app/app/docs
          restartPolicy: OnFailure
          volumes:
          - name: pydocuments-data
            persistentVolumeClaim:
              claimName: pydocument-pvc
